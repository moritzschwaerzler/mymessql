# MyMessQL

Es soll ein Programm entwickelt werden, welches es ermöglicht Messergebnisse auf einer MySQL Datenbank zu verwalten.
Diese Messergebnisse sollen einfach darstellbar sein, sowohl als Tabelle oder als Plot bzw Graph.




Die Software wird grundsätzlich in zwei Blöcke aufgeteilt: das Dataset und die Anwendung. Das Dataset beinhaltet die Adapter und lokale Kopien für die Tabellen und regelt die gesamte Kommunikation mit dem MySQL-Server. Über die Anwendung werden die Daten dargestellt und bieten dem User die gewünschten Operationen, wie neue Daten speichern usw.

![image](pics/1.png)


**Dataset/Datenbank Zugriff**

Das Dataset/Datenbank beinhaltet zwei Tabellen und eine View, welche diese zwei verbindet bzw. verknüpft. Die View wird aber im Programm nicht verwendet, da es einfacher ist die getrennten Tabellen zu betrachten und vor allem zu bearbeiten. In den jeweiligen Adaptern können Methoden/Abfragen definiert werden, welche die geforderten Operationen vereinfachen/ermöglichen.

![image](pics/2.png)

Auf der Datenbank liegen die beiden Tabellen: "main" und "werte". In main sind Eintragungen der Messreihen, wo der Titel und die drei Überschriften als varchar gespeichert werden (+ID). In werte sind die tatsächlichen Messpunkte mit drei Feldern für die Werte und eine ReferenzID, welche auf eine ID aus der main Tabelle zeigt (+ID).


**MVVM**

Das MVVM beinhaltet beinahe die gesamte Logik des Projektes. Von hier aus wird auf die Tabellenadapter und Tabellen zugegriffen und verarbeitet. Alle Daten werden auf ein, zum Programmstart erstellten Objekt dieser Type gebindet. Ebenso werden alle Befehle aus den Fenstern vom mvvm verarbeitet.

![image](pics/3.png)

Diese Klasse erbt von dem Interface INotifyPropertyChanged, um bei Änderungen der Properties das Binding die Werte aktualisiert. Für die diversen Bindings gibt es wie erwähnt Properties, welche die Daten auf die jeweiligen Felder speichert. Zudem werden wie erwähnt die nötigen Methoden zur Verfügung gestellt. Es wurde zudem ein Event hinzugefügt: NamenChange. Da sich das Binding der strings für die jeweiligen Überschriften der Reihen als schwierig erwiesen hat, wurde dies implementiert. Wenn sich nun die Auswahl der Messreihe ändert und sich deshalb die Überschriften ändern, wird das NamenChange Event ausgelöst. Um die drei strings dabei übertragen zu können, werden NameEventArgs definiert:


**NameEventArgs**

Diese Klasse erbt von EventArgs und hat drei Properties für die drei Überschriften.

![image](pics/4.png)


**MainWindow**

Vom Startfenster aus können alle weiteren Fenster für Eingaben geöffnet werden. Über die Combobox kann eine der existierenden Messreihen ausgewählt und angezeigt werden. Mit der Taste 'N' kann zu der geöffneten Messreihe ein Messpunkt eingefügt werden. Dazu öffnet sich ein Eingabefeld, in das die Werte eingetragen werden können. Wird eine Zeile ausgewählt/angeklickt, kann diese mit der Taste 'E' bearbeitet werden. Eine neue Messung kann über 'Enter' oder den Button Neue Messung hinzugefügt werden. Dazu wird ebenso ein Fenster zur Abfrage der Werte geöffnet. Weiter kann aus dem Fenster mit dem "ganze Messung löschen" - Knopf die ausgewählte Messreihe und alle Werte dazu gelöscht werden. Bei einer ausgewählten Zeile kann die Taste 'D' verwendet werden, um diese zu löschen. Mit der Taste 'G' kann zu der geöffneten Messreihe ein Graph gezeichnet werden, dazu wird ein weiteres Fenster geöffnet.

![image](pics/5.png)

Da die Daten fast alle gebindet werden, ist nur wenig Code für die Fenster notwendig. Bis auf das abonnierte Event für die Überschriften sind die Methoden auf das Behandeln der Events aus dem Fenster begrenzt. Diese öffnen, wenn nötig, ein Fenster zur Abfrage von Daten und lösen im mvvm die nötigen Operationen aus.

![image](pics/6.png)


**NeueMessung**

Dieses Fenster wird verwendet, um die Daten einer neuen Messreihe zu erfassen (Titel, Überschriften der Spalten). Es wird auch verwendet, um eine Messreihe zu bearbeiten.

![image](pics/7.png)

Aufgrund des Bindings konnte der Code in diesem Fenster auf die Buttonevents beschränkt werden.

![image](pics/8.png)


**InsertUpdate**

Mit diesem Fenster können die Werte einer Messung bearbeitet werden oder eine neue Messung hinzugefügt werden. Dazu kann man die drei Parameter bearbeiten.

![image](pics/9.png)

Wie zuvor ist in diesem Fenster aufgrund des Bindings nur wenig Code notwendig.

![image](pics/10.png)


**DatenDarstellen**

Mit diesem Fenster können die Datenpunkte einer Messreihe in einem 3D Graphen dargestellt werden. Durch die Auswahl, welche Dimension dargestellt wird, können hier 3D und 2D Graphen gezeichnet werden.

![image](pics/11.png)

Durch das Binding gibt es auch hier beinahe keinen Aufwand. Durch Änderungen an den Checkboxen werden die Werte/Punkte automatisch geladen.

![image](pics/12.png)
