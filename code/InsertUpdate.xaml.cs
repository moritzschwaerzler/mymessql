﻿using System.Windows;

namespace MyMessQL
{
    /// <summary>
    /// Interaktionslogik für InsertUpdate.xaml
    /// </summary>
    public partial class InsertUpdate : Window
    {
        mvvm mvvm;
        public InsertUpdate(ref mvvm mvvmref)
        {
            InitializeComponent();
            mvvm = mvvmref;
            DataContext = mvvm;
        }

        private void btsave_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void btcancle_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
