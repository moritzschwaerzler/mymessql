﻿using System.Windows;

namespace MyMessQL
{
    /// <summary>
    /// Interaktionslogik für DatenDarstellen.xaml
    /// </summary>
    public partial class DatenDarstellen : Window
    {
        mvvm mvvm;
        public DatenDarstellen(ref mvvm mvvmref)
        {
            InitializeComponent();
            mvvm = mvvmref;
            DataContext = mvvm;
            mvvm.GenPoints();
        }

        private void btclose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
