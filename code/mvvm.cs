﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media.Media3D;

namespace MyMessQL
{
    public class mvvm : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }

        private string title;
        private string name1;
        private string name2;
        private string name3;
        private int refid;
        private string value1;
        private string value2;
        private string value3;
        private int coboindex;
        private int selectedid;
        private Point3DCollection pc3d;
        private Point3D x1;
        private Point3D x2;
        private Point3D y1;
        private Point3D y2;
        private Point3D z1;
        private Point3D z2;
        private double xmin;
        private double xmax;
        private double ymin;
        private double ymax;
        private double zmin;
        private double zmax;
        private bool? check1;
        private bool? check2;
        private bool? check3;
        private string title0;
        private string name10;
        private string name20;
        private string name30;

        public bool? ViewCheck3
        {
            get { return check3; }
            set { check3 = value; GenPoints(); OnPropertyChanged("ViewCheck3"); }
        }


        public bool? ViewCheck2
        {
            get { return check2; }
            set { check2 = value; GenPoints(); OnPropertyChanged("ViewCheck3"); }
        }


        public bool? ViewCheck1
        {
            get { return check1; }
            set { check1 = value; GenPoints(); OnPropertyChanged("ViewCheck3"); }
        }


        public Point3D X1
        {
            get { return x1; }
            set { x1 = value; OnPropertyChanged("X1"); }
        }
        public Point3D Y1
        {
            get { return y1; }
            set { y1 = value; OnPropertyChanged("Y1"); }
        }
        public Point3D X2
        {
            get { return x2; }
            set { x2 = value; OnPropertyChanged("X2"); }
        }
        public Point3D Y2
        {
            get { return y2; }
            set { y2 = value; OnPropertyChanged("Y2"); }
        }
        public Point3D Z1
        {
            get { return z1; }
            set { z1 = value; OnPropertyChanged("Z1"); }
        }
        public Point3D Z2
        {
            get { return z2; }
            set { z2 = value; OnPropertyChanged("Z2"); }
        }


        public Point3DCollection PC3D
        {
            get { return pc3d; }
            set { pc3d = value; OnPropertyChanged("PC3D"); }
        }


        public int SelectedID
        {
            get { return selectedid; }
            set { selectedid = value; RefreshWerteTable(); OnPropertyChanged("SelectedID"); }
        }


        public int CoBoIndex
        {
            get { return coboindex; }
            set { coboindex = value; RefreshSelectedID(); OnPropertyChanged("CoBoIndex"); }
        }


        public string Value3
        {
            get { return value3; }
            set { value3 = value; OnPropertyChanged("Value3"); }
        }

        public string Value2
        {
            get { return value2; }
            set { value2 = value; OnPropertyChanged("Value2"); }
        }

        public string Value1
        {
            get { return value1; }
            set { value1 = value; OnPropertyChanged("Value1"); }
        }

        public int RefID
        {
            get { return refid; }
            set { refid = value; OnPropertyChanged("RefID"); }
        }


        public string Name3
        {
            get { return name3; }
            set { name3 = value; OnPropertyChanged("Name3"); }
        }

        public string Name2
        {
            get { return name2; }
            set { name2 = value; OnPropertyChanged("Name2"); }
        }

        public string Name1
        {
            get { return name1; }
            set { name1 = value; OnPropertyChanged("Name1"); }
        }

        public string Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }


        public MyDBTableAdapters.mainTableAdapter mainAdapter = new MyDBTableAdapters.mainTableAdapter();
        public MyDBTableAdapters.werteTableAdapter werteAdapter = new MyDBTableAdapters.werteTableAdapter();
        public MyDBTableAdapters.messviewTableAdapter messviewAdapter = new MyDBTableAdapters.messviewTableAdapter();

        public MyDB db = new MyDB();

        public MyDB.mainDataTable mainTable
        {
            get;
            set;
        }
        public MyDB.werteDataTable werteTable
        {
            get;
            set;
        }
        public MyDB.messviewDataTable messviewTable
        {
            get;
            set;
        }


        public mvvm()
        {
            mainTable = db.main;
            werteTable = db.werte;
            messviewTable = db.messview;

            mainAdapter.Fill(mainTable);
            //messviewAdapter.Fill(messviewTable);
            //werteAdapter.Fill(werteTable);

            CoBoIndex = 0;
            ViewCheck3 = false;
            ViewCheck2 = false;
            ViewCheck1 = false;
        }

        public void FillAlle()
        {
            werteAdapter.Fill(werteTable);
            mainAdapter.Fill(mainTable);
        }

        public void InsertWerte(int refid)
        {
            werteAdapter.InsertQuery(refid, Value1, Value2, Value3);
        }

        public void GenPoints()
        {
            try
            {
                var rows = werteTable.Rows;
                int count = rows.Count;
                PC3D = new Point3DCollection();
                xmin = -5.0;
                xmax = 5.0;
                ymin = -5.0;
                ymax = 5.0;
                zmin = -5.0;
                zmax = 5.0;

                double x = 0.0;
                double y = 0.0;
                double z = 0.0;

                for (int i = 0; i < count; i++)
                {
                    var row = rows[i];

                    var arr = row.ItemArray;

                    if (ViewCheck1 == true)
                    {
                        x = Convert.ToDouble(arr[2]);
                        if (x < xmin) { xmin = x; }
                        if (x > xmax) { xmax = x; }
                    }
                    else
                    {
                        x = 0.0;
                    }

                    if (ViewCheck2 == true)
                    {
                        y = Convert.ToDouble(arr[3]);
                        if (y < ymin) { ymin = y; }
                        if (y > ymax) { ymax = y; }
                    }
                    else
                    {
                        y = 0.0;
                    }

                    if (ViewCheck3 == true)
                    {
                        z = Convert.ToDouble(arr[4]);
                        if (z < zmin) { zmin = z; }
                        if (z > zmax) { zmax = z; }
                    }
                    else
                    {
                        z = 0.0;
                    }

                    PC3D.Add(new Point3D(x, y, z));
                }

                X1 = new Point3D(xmin, 0, 0);
                X2 = new Point3D(xmax, 0, 0);
                Y1 = new Point3D(0, ymin, 0);
                Y2 = new Point3D(0, ymax, 0);
                Z1 = new Point3D(0, 0, zmin);
                Z2 = new Point3D(0, 0, zmax);

                OnPropertyChanged("PC3D");
            }
            catch (Exception exp)
            {
                MessageBox.Show("Fehler beim generieren der Punkte: " + exp.Message);
                PC3D = null;
            }
        }

        public void UpdateWert(int id)
        {
            werteAdapter.UpdateQuery(Value1, Value2, Value3, id);
        }

        public void InsertMain()
        {
            mainAdapter.InsertQuery(Title, Name1, Name2, Name3);
            mainAdapter.Fill(mainTable);
            CoBoIndex = mainTable.Rows.Count - 1;
        }

        public void RefreshSelectedID()
        {
            if (CoBoIndex >= 0)
            {
                try
                {

                    SelectedID = int.Parse(mainTable.Rows[CoBoIndex].ItemArray[0].ToString());
                }
                catch (Exception exp)
                {
                    MessageBox.Show(exp.Message);
                }
            }
        }

        public void EditValues(int i)
        {
            int id = Convert.ToInt32(werteTable.Rows[i].ItemArray[0]);
            UpdateWert(id);
            werteAdapter.FillBy(werteTable, SelectedID);
        }

        public void DeleteValue(int index)
        {
            int i = Convert.ToInt32(werteTable.Rows[index].ItemArray[0]);
            werteAdapter.DeleteQuery(i);
            werteAdapter.FillBy(werteTable, SelectedID);
        }

        public void NewRow()
        {
            InsertWerte(SelectedID);
            werteAdapter.FillBy(werteTable, SelectedID);
        }

        public void RefreshWerteTable()
        {
            if (SelectedID > 0)
            {
                werteAdapter.FillBy(werteTable, SelectedID);
                var arr = mainTable.Rows[CoBoIndex].ItemArray;
                Title = arr[1].ToString();
                Name1 = arr[2].ToString();
                Name2 = arr[3].ToString();
                Name3 = arr[4].ToString();
                OnNamenChange(new NameEventArgs() { Name1 = this.Name1, Name2 = this.Name2, Name3 = this.Name3 });
            }
        }

        public void FillZeichna(int index)
        {
            double a = 0.0;
            switch (index)
            {
                case 0:
                    a = 0.0;
                    for (int i = 0; i < 200; i++)
                    {
                        Value1 = Convert.ToString(2 * Math.Sin(a));
                        Value2 = Convert.ToString(2 * Math.Cos(a));
                        Value3 = Convert.ToString(-2 * Math.Sin(a));
                        NewRow();
                        a++;
                    }
                    break;
                case 9:
                    a = 0.0;

                    for (int i = 0; i < 200; i++)
                    {
                        Value1 = Convert.ToString(3 * Math.Sin(3 * a) * Math.Sin(a));
                        Value2 = Convert.ToString(3 * Math.Sin(3 * a) * Math.Cos(a));
                        Value3 = Convert.ToString(3 * Math.Cos(3 * a) * Math.Cos(a));
                        NewRow();
                        a++;
                    }
                    break;
                case 8:
                    a = 0.0;
                    Random rand = new Random(123321);
                    for (int i = 0; i < 200; i++)
                    {
                        Value1 = Convert.ToString(2 * Math.Sin(rand.NextDouble()*a)+5);
                        Value2 = Convert.ToString(5 * Math.Cos(rand.NextDouble()*a)+5);
                        Value3 = Convert.ToString(-8 * Math.Sin(rand.NextDouble()*a)+5);
                        NewRow();
                        a++;
                    }
                    break;
                case 7:
                    a = 0.0;

                    for (int i = 0; i < 200; i++)
                    {
                        Value1 = Convert.ToString(3 * Math.Sin(5 * a) * Math.Sin(3 * a) * Math.Cos(a));
                        Value2 = Convert.ToString(5 * Math.Cos(5 * a) * Math.Cos(3 * a) * Math.Sin(a));
                        Value3 = Convert.ToString(7 * Math.Cos(5 * a) * Math.Cos(3 * a) * Math.Cos(a));
                        NewRow();
                        a++;
                    }
                    break;
            }
        }

        public void FlushValueStrings()
        {
            Value1 = "";
            Value2 = "";
            Value3 = "";
        }

        public void SaveMainLocal()
        {
            title0 = Title;
            name10 = Name1;
            name20 = Name2;
            name30 = Name3;
        }

        public void FlushMain()
        {
            SaveMainLocal();
            Title = "";
            Name1 = "";
            Name2 = "";
            Name3 = "";
        }

        public void RefillMain()
        {
            Title = title0;
            Name1 = name10;
            Name2 = name20;
            Name3 = name30;
        }

        public void UpdateMain()
        {
            int i = CoBoIndex;
            mainAdapter.UpdateQuery(Title, Name1, Name2, Name3, SelectedID);
            mainAdapter.Fill(mainTable);
            CoBoIndex = 0;
            CoBoIndex = i;
        }

        public void DeleteMeasure()
        {
            werteAdapter.DeleteQuery1(SelectedID);
            mainAdapter.DeleteQuery(SelectedID);
            CoBoIndex = 0;
            werteAdapter.FillBy(werteTable, SelectedID);
            mainAdapter.Fill(mainTable);
        }

        public void FillValueStrings(int selectedIndex)
        {
            var arr = werteTable.Rows[selectedIndex].ItemArray;
            Value1 = arr[2].ToString();
            Value2 = arr[3].ToString();
            Value3 = arr[4].ToString();
        }

        public event EventHandler<NameEventArgs> NamenChange;

        protected virtual void OnNamenChange(NameEventArgs eventargs)
        {
            NamenChange?.Invoke(this, eventargs);
        }
    }

    public class NameEventArgs : EventArgs
    {
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
    }
}
