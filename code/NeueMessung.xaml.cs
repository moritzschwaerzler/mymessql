﻿using System.Windows;

namespace MyMessQL
{
    /// <summary>
    /// Interaktionslogik für NeueMessung.xaml
    /// </summary>
    public partial class NeueMessung : Window
    {
        mvvm mvvm;
        public NeueMessung(ref mvvm mvvmref)
        {
            InitializeComponent();
            mvvm = mvvmref;
            DataContext = mvvm;
        }

        private void btsave_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void btstop_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
