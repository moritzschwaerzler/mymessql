﻿using System.Windows;
using System.Windows.Input;

namespace MyMessQL
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        mvvm mvvm;
        public MainWindow()
        {
            InitializeComponent();
            mvvm = new mvvm();
            DataContext = mvvm;
            mvvm.NamenChange += Mvvm_NamenChange;
            mvvm.RefreshWerteTable();
        }

        private void Mvvm_NamenChange(object sender, NameEventArgs e)
        {
            col1.Header = e.Name1;
            col2.Header = e.Name2;
            col3.Header = e.Name3;
        }

        private void btneue_Click(object sender, RoutedEventArgs e)
        {
            NeueMessung win = new NeueMessung(ref mvvm);
            mvvm.FlushMain();
            if (win.ShowDialog() == true)
            {
                mvvm.InsertMain();
            }
            else
            {
                mvvm.RefillMain();
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            InsertUpdate win;

            switch (e.Key)
            {
                case Key.E:
                    int i = spezidatagrid.SelectedIndex;
                    if (i >= 0)
                    {
                        mvvm.FillValueStrings(i);
                        win = new InsertUpdate(ref mvvm);
                        if (win.ShowDialog() == true)
                        {
                            mvvm.EditValues(i);
                        }
                    }
                    break;
                case Key.D:
                    int j = spezidatagrid.SelectedIndex;
                    if (j >= 0)
                    {
                        if (MessageBoxResult.Yes == MessageBox.Show("Soll die ausgewählte Messung gelöscht werden?", "Messung löschen", MessageBoxButton.YesNo))
                        {
                            mvvm.DeleteValue(j);
                        }
                    }
                    break;
                case Key.N:
                    mvvm.FlushValueStrings();
                    win = new InsertUpdate(ref mvvm);
                    if (win.ShowDialog() == true)
                    {
                        mvvm.NewRow();
                    }
                    break;
                case Key.G:
                    DatenDarstellen gwin = new DatenDarstellen(ref mvvm);
                    gwin.ShowDialog();
                    break;
                case Key.D0:
                    mvvm.FillZeichna(0);
                    break;
                case Key.D9:
                    mvvm.FillZeichna(9);
                    break;
                case Key.D8:
                    mvvm.FillZeichna(8);
                    break;
                case Key.D7:
                    mvvm.FillZeichna(7);
                    break;
                case Key.A:
                    spezidatagrid.SelectAll();
                    break;
            }
        }

        private void bthelp_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Mit der Taste 'E' kann die ausgewählte Zeile bearbeitet werden. Weiter kann mit der Taste 'N' eine neue Zeile erstellt werden. Über 'Enter' kann eine neue Messung hinzugefügt werden. Über 'G' kann ein Graph gezeichnet werden. Die Werte der Tabelle können kopiert und eine andere Datei eingefügt werden. Über die Taste 'D' kann eine ausgewählte Zeile gelöscht werden. Mit der Taste 'A' können alle Zellen markiert werden.");
        }

        private void btclose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btdeletemeasure_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBoxResult.Yes == MessageBox.Show("Sind Sie sicher diese Messung und alle damit verbundenen Messpunkte löschen zu wollen?", "Messung löschen", MessageBoxButton.YesNo))
            {
                mvvm.DeleteMeasure();
            }
        }

        private void bteditmeasure_Click(object sender, RoutedEventArgs e)
        {
            NeueMessung win = new NeueMessung(ref mvvm);
            mvvm.SaveMainLocal();
            if (win.ShowDialog() == true)
            {
                mvvm.UpdateMain();
            }
            else
            {
                mvvm.RefillMain();
            }
        }
    }
}
